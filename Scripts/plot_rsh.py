#! /usr/bin/python

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import math
import rsh

def sample_sphere(theta, phi):
	x = math.sin(theta) * math.cos(phi)
	y = math.sin(theta) * math.sin(phi)
	z = math.cos(theta)
	return x, y, z

sample_count = 200

thetas = np.linspace(0.0, np.pi, sample_count)
phis = np.linspace(0.0, 2.0 * np.pi, sample_count)

xs = np.empty((sample_count, sample_count))
ys = np.empty((sample_count, sample_count))
zs = np.empty((sample_count, sample_count))

for i in range(0, sample_count):
	for j in range(0, sample_count):
		theta = thetas[j]
		phi = phis[i]
		x, y, z = sample_sphere(theta, phi)
		r = math.fabs(rsh.Y(3, -2, theta, phi))
		#r = 1.0
		xs[i][j] = r * x;
		ys[i][j] = r * y;
		zs[i][j] = r * z;

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlim3d([-1.0, 1.0])
ax.set_ylim3d([-1.0, 1.0])
ax.set_zlim3d([-1.0, 1.0])
ax.plot_surface(xs, ys, zs, rstride = 4, cstride = 4)
plt.show()
