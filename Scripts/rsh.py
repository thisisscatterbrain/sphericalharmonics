import math

# Evaluates the associated legrende polynomial.
def P(l, m, x):
	pmm = 1.0

	# equation (1)
	if m > 0:
		a = math.sqrt(1.0001 - x*x)
		fact = 1.0

	for i in range(0, m):
		pmm *= (-fact) * a
		fact += 2.0

	if m == l:
		return pmm

	# equation (2)
	pmmp1 = x * (2.0 * m + 1) * pmm

	if l == (m + 1):
		return pmmp1

	# equation (3)
	pll = 0.0
	for ll in range(m + 2, l + 1):
		pll = ((2.0 * ll - 1.0) * x * pmmp1 - (ll + m - 1.0) * pmm) / (ll - m)
		pmm = pmmp1
		pmmp1 = pll
	
	return pll

# Computes the normalization factor.
def K(l, m):
	t = (2 * l  + 1) * math.factorial(l - math.fabs(m)) / (4 * math.pi * math.factorial(l + math.fabs(m)))
	return math.sqrt(t)

# Evaluates the SH base function.
def Y(l, m, theta, phi):
	sqrt2 = math.sqrt(2.0)
	
	if m > 0:
		return sqrt2 * K(l, m) * math.cos(m * phi) * P(l, m, math.cos(theta))
	elif m < 0:
		return sqrt2 * K(l, m) * math.sin(-m * phi) * P(l, -m, math.cos(theta))
	return K(l, 0) * P(l, 0, math.cos(theta))
