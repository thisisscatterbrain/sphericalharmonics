#! /usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import rsh

sample_count = 100

x = np.linspace(-1.0, 1.0, sample_count)
y = np.empty(sample_count)

for i in range(0, sample_count):
	y[i] = rsh.P(1, 0, x[i])

plt.plot(x, y)
plt.show()
